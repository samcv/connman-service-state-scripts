# Connman Service State Scripts#
**connman-service-state-scripts** is a patch to the [ConnMan](https://01.org/connman), a light and low resources connection manager.  
The patch will allow you to run scripts that are triggered by the following service states in ConnMan:
Online, Disconnect, Unknown, Idle, Failure and Ready.  The default script location is in /etc/connman/scripts and the scripts are 
named after the states that they are for.  For example, `/etc/connman/scripts/online` is the script for when connman enters 
the online state, and `/etc/connman/scripts/disconnect` for when ConnMan enters the disconnect state.
The other state's scripts are similarly named after the service states which trigger them, as listed above(online, disconnect, unknown, idle, failure, and ready).  The scripts 
will only be attempted to run if the file both exists and it has executable permissions set.
To easily disable a script just run `chmod -X /etc/connman/scripts/ready` and it will be disabled, and 
ConnMan does not need to be restarted for this to take effect.
In addition, launching the scripts will not block the execution of the ConnMan program either 
and it is fine if the script executables are binary files and not shell scripts.

The license for my patch is **GPLv2**, and this is the same license used by the ConnMan project.

## State Descriptions

More information on network states can also be found here in the ConnMan [documentation](https://git.kernel.org/cgit/network/connman/connman.git/tree/doc/overview-api.txt) about halfway through the document.
**Ready:** ConnMan tries to detect if it has Internet connection or not when
a service is connected. If the online check succeeds the service
enters Online state, if not it stays in Ready state. The online
check is also used to detect whether ConnMan is behind a captive
portal like when you are in hotel and need to pay for connectivity.

**Note** While connecting the **Ready** state can be called multiple 
times in rapid succession as the device is connecting.  Once it is actually online it will move to the Online state, which will not be called multiple times.

**Online:** ConnMan has detected that you have internet connectivity.
